import { model } from './model';
import { templates } from './templates';
import './styles/main.css';

const app = document.querySelector('#app');


model.forEach(block => {
  const generate = templates[block.type];
  // console.log(generate);

  if (generate) {
    const html = generate(block);
    app.insertAdjacentHTML('beforeend', html);
  }
});

