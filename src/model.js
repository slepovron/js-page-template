export const model = [{
  type: 'jumbotron',
  title: 'test title',
  text: 'some text',
  options: {
    styles: 'background: grey; color: white;' // some styles will be added inline in html tag
  }
},
{
  type: 'navs',
  value: [
    'link1',
    'link2',
    'link3'
  ],
  options: {}
},
{
  type: 'cards',
  value: [
    '1 card',
    '2 card',
    '3 card',
    '1 card',
    '2 card',
    '3 card'
  ],
  options: {}
}
];