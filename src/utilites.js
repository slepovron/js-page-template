export function jumbo(content, styles = '') {
  return `<div class="jumbotron" style="${styles}">${content}</div>`;
}

export function container(content) {
  return `<div class="container">${content}</div>`;
}