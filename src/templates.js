import { jumbo, container } from "./utilites";

function jumbotron(block) {
  return jumbo(
    container(`
      <h1 class="display-4">${block.title}</h1>
      <p class="lead">${block.text}</p>
    `),
    block.options.styles
  );
}

function navs(block) {
  console.log(block);
  const html = block.value.map(
    (item) => `
    <li class="nav-item">
      <a class="nav-link" href="#">${item}</a>
    </li>
  `
  );
  return `
  <ul class="nav">
    ${html.join("")}
  </ul>
  `;
}

function cards(block) {
  const html = block.value.map(
    (item) => `
    <div class="card">${item}</div>
    `
  );
  return `
  <div class="card-columns">
   ${html.join("")}
  </div>
  `;
}

export const templates = {
  jumbotron,
  navs,
  cards,
};
